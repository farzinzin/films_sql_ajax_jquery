// Auteur: Farzin Faridfar
// Cours: IFT1147 Programmation serveur Web avec PHP
// Date: 18 Dec 2016
// Travail paratique #3
// Fichier: requetesMembre.js
// Desc: Les requetes ajax envoye a la base de donnees
// et au tableau membres

function visible(elem)
{
  document.getElementById(elem).style.display='block';
}

function invisible(elem)
{
  document.getElementById(elem).style.display='none';
}

function req_ajoutMem(){
  //alert("req_ajoutMem()");
  $.ajax({
    type: "POST",
    url: "gestionnaire/gestionMembre.php",
    data: $("#formAjoutMem").serialize(),
    dataType:"json",
    //dataType:"text",
    success:function(tabJson){
      // alert("req_ajoutMem()"+JSON.stringify(tabJson));
      
      $("#msg").html(tabJson.msg);
      $("#msg").show();
      setInterval(function(){ $("#msg").hide(); }, 3000);
    },
    error : function(err){
      $("#msg").html(err);
      $("#msg").show();
    }
  });
}

