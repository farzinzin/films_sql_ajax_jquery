// Auteur: Farzin Faridfar
// Cours: IFT1147 Programmation serveur Web avec PHP
// Date: 18 Dec 2016
// Travail paratique #3
// Fichier: requetesFilm.js
// Desc: Les requetes ajax envoye a la base de donnees
// et au tableau films

function visible(elem)
{
  document.getElementById(elem).style.display='block';
}

function invisible(elem)
{
  document.getElementById(elem).style.display='none';
}

//pour vider l'input d'enregistrer et enlever
//apres avoir appuye sur le bouton d'envoye
function vider(elem)
{
  document.getElementById(elem).value='';
}

function req_lister(){
  //alert("req_lister()");
  $.ajax({
    type: "POST",
    url: "../gestionnaire/gestionFilm.php",
    data: {"action":"lister"},
    dataType:"json",
    success:function(tabJson){
      lister(tabJson);
    },
    error : function(err){
    }
  });
}

function req_enreg(){
  $.ajax({
    type: "POST",
    url: "../gestionnaire/gestionFilm.php",
    data: $("#formEnreg").serialize(),
    dataType:"json",
    success:function(tabJson){
      $("#msg").html(tabJson.msg);
      $("#msg").show();
      setInterval(function(){ $("#msg").hide(); }, 3000);
    },
    error : function(err){
    }
  });
}

function req_enlever()
{

  //alert("req_enreg_panier()");
  $.ajax({
    type: "POST",
    url: "../gestionnaire/gestionFilm.php",
    data: $("#formEnlever").serialize(),
    dataType:"json",
    //dataType:� "text" �,
    success:function(tabJson){
      //alert(JSON.stringify(tabJson));
      $("#msg").html(tabJson.msg);
      $("#msg").show();
      setInterval(function(){ $("#msg").hide(); }, 3000);
    },
    error : function(err){
    }
  }); 
}

function req_modifier_fiche(){
  $.ajax({
    type: "POST",
    url: "../gestionnaire/gestionFilm.php",
    data: $("#formModifier").serialize(),
    dataType:"json",
    //dataType: "text" �,
    success:function(tabJson){
      afficherDossier(tabJson);
      //alert(JSON.stringify(tabJson));
    },
    error : function(err){
    }
  });
}

function req_maj(){
  $.ajax({
    type: "POST",
    url: "../gestionnaire/gestionFilm.php",
    data: $("#formModif").serialize(),
    dataType:"json",
    // dataType:�"text" �,
    success:function(tabJson){
      //alert("req_maj()"+JSON.stringify(tabJson));

      $("#msg").html(tabJson.msg);
      $("#msg").show();
      setInterval(function(){ $("#msg").hide(); }, 3000);
    },
    error : function(err){
    }
  });
}

//********************** Fonction auxiliaires **********
function lister(tabJson){//alert(JSON.stringify(tabJson));
  var rep="";
  rep+="<table>";
  var taille=tabJson.length;
  var ligne=null;
  rep+="<tr><th>ID Film</th><th>Titre</th><th>Realisateur</th><th>Categorie</th><th>Duree</th><th>Prix</th><th>Image</th></tr>";
  for(var i=0;i<taille;i++){
    ligne=tabJson[i];
    rep+="<tr><td>"+ligne['idfilm']+"</td>";
    rep+="<td>"+ligne['titre']+"</td>";
    rep+="<td>"+ligne['realisateur']+"</td>";
    rep+="<td>"+ligne['categorie']+"</td>";
    rep+="<td>"+ligne['duree']+"</td>";
    rep+="<td>"+ligne['prix']+"</td>";
    // rep+="<td><img src=\"../images/image_"+ligne['image']+".jpg\" style='width:65px;height:95px;'></td></tr>";
    rep+="<td><a href=\"../images/image_"+ligne['image']+".jpg\"><video id="+ligne['image']+" poster=\"../images/image_"+ligne['image']+".jpg\" width='65'; height='95'></video></a></td></tr>";

  }
  rep+="</table>";
  $("#contenu").html(rep);
  $("#fenetre").show();
}

function afficherDossier(tabJson){//alert(JSON.stringify(tabJson));
  var rep="";
  var cat = tabJson['categorie'];
  var categories = ["Action", "Drame et R&eacutepertoire", "Com�die", "Sience Fiction", "Horreur", "Suspense", "Com�dies Musicales", "Pour le temps des F�tes", "Qu�b�cois", "Pour la famille"];
  var img = tabJson['image'];
  var images = ["1","2","3","4","5","6","7","8","9","10"];

  rep+="<form id=\"formModif\"><br>"; 
  rep+="<h3>Mettre &agrave jour un film</h3><br>"; 
  
  rep+= "ID Film:<br><input type='text'  class='modIn' id=\"idmaj\" name=\"idmaj\" value='"+tabJson['idfilm']+"'></br>"; 
  rep+= "Titre:<br><input type='text'  class='modIn' id=\"titre\" name=\"titre\" value='"+tabJson['titre']+"'></br>"; 
  rep+= "R&eacutealisateur:<br><input type='text'  class='modIn' id=\"realisateur\" name=\"realisateur\" value='"+tabJson['realisateur']+"'></br>"; 
  rep+= " Cat&eacutegorie:<br>"; 
  rep+= "  <select class='selModif' id=\"categorie\" name=\"categorie\">"; 
    categories.forEach(function(item, index)
        {
            if(item == cat)
               rep+="	    <option name=\""+cat+"\" selected>"+cat+"</option>\n"; 
            else
                rep+= "	    <option name=\""+item+"\">"+item+"</option>\n"; 
        });
  rep+= "  </select><br>"; 
  rep+= "Dur&eacutee:<br><input type='text' class='modIn' id=\"duree\" name=\"duree\" value='"+tabJson['duree']+"'> min</br>"; 
  rep+= "Prix:<br><input type='text' class='modIn' id=\"prix\" name=\"prix\" value='"+tabJson['prix']+"'> $</br>"; 
  rep+= "Image:<br>"; 
  rep+= "  <select class='selModif' id=\"image\" name=\"image\">"; 
    images.forEach(function(item, index)
        {
            if(item == img)
               rep+="	    <option name=\""+img+"\" selected>"+img+"</option>\n"; 
            else
                rep+= "	    <option name=\""+item+"\">"+item+"</option>\n"; 
        });
  rep+= " </select>"; 
  rep+= "</div>"; 

  rep+="<input type=\"hidden\" name=\"action\" value=\"modifier\"><br>";
  rep+="<input type=\"hidden\" name=\"code\" value=\"maj\"><br>"; 	
  rep+="<input type=\"button\" value=\"Modifier\" onClick=\"req_maj();\"><br>"; 
  rep+="</form><br>";
  $("#divModifier").hide();
  $("#contenu").html(rep);
  $("#fenetre").show();
}

