// Farzin Faridfar
// Source: 10-2-GestionMembres_PHP_MySQL_POO_MODELE_SESSIONS, valider.js
// Cours: IFT1147 Programmation serveur Web avec PHP
// Date: 18 Dec 2016
// Travail paratique #3
// Fichier: valider.js
// Desc: La verification des entree recues


function valider(formulaire){

  var expReg_num=/^[0-9]{3}$/;
  var expReg_tel = /^[0-9]{10}$/;
  var expReg_courriel = /^[a-zA-Z0-9_-]+@[a-zA-Z0-9-]{2,}[.][a-zA-Z]{2,3}$/;
  var expReg_pass = /^[a-zA-Z0-9]{8,10}$/;

  var num=eval(formulaire).num.value;
  var tel=eval(formulaire).tel.value;
  var courriel=eval(formulaire).courriel.value;
  var pass=eval(formulaire).pass.value;
  var cpass=eval(formulaire).cpass.value;
  eval(formulaire).i2.src="../images/ok.gif";
  eval(formulaire).i2.style.visibility='visible';
  var etat=true;
  var img1=eval(formulaire).i1;
  var img3=eval(formulaire).i3;
  var img4=eval(formulaire).i4;
  var img5=eval(formulaire).i5;
  var img6=eval(formulaire).i6;
  //Tester le num�ro du membre
  if(expReg_num.test(num))
    img1.src="../images/ok.gif";
  else
  {
    img1.src="../images/pasok.gif";
    etat=false;
  }
  img1.style.visibility='visible';
  //Tester mot de passe
  if (pass==cpass && expReg_pass.test(pass)){
    img5.src="../images/ok.gif";
    img6.src="../images/ok.gif";
  }
  else
  {
    img5.src="../images/pasok.gif";
    img6.src="../images/pasok.gif";
    etat=false;
  }
  img5.style.visibility='visible';
  img6.style.visibility='visible';
  //Tester le num�ro de t�l�phone du membre
  if(expReg_tel.test(tel))
    img3.src="../images/ok.gif";
  else
  {
    img3.src="../images/pasok.gif";
    etat=false;
  }
  img3.style.visibility='visible';

  //Tester le courriel du membre
  if(expReg_courriel.test(courriel))
    img4.src="../images/ok.gif";
  else
  {
    img4.src="../images/pasok.gif";
    etat=false;
  }
  img4.style.visibility='visible';

  //Si tout OK envoyer au serveur
  if(etat)
    eval(formulaire).submit();
  else
    return false;
}