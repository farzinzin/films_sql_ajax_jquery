// Auteur: Farzin Faridfar
// Cours: IFT1147 Programmation serveur Web avec PHP
// Date: 18 Dec 2016
// Travail paratique #3
// Fichier: requetesPanier.js
// Desc: Les requetes ajax envoye a la base de donnees
// et au tableau panier

//pour vider l'input d'enregistrer et enlever
//apres avoir appuye sur le bouton d'envoye
function vider(elem)
{
  document.getElementById(elem).value='';
}

function req_lister_panier(){
  //alert("req_lister()");
  $.ajax({
    type: "POST",
    url: "../gestionnaire/gestionPanier.php",
    data: {"action":"listerPanier"},
    dataType:"json",
    success:function(tabJson){
      lister(tabJson);
    },
    error : function(err){
    }
  });
}

function req_enreg_panier()
{
  //alert("req_enreg_panier()");
   $.ajax({
    type: "POST",
    url: "../gestionnaire/gestionPanier.php",
    data: $("#formEnregPanier").serialize(),
    dataType:"json",
    //dataType:  "text"  ,
    success:function(tabJson){
      //alert(JSON.stringify(tabJson));
      $("#msg").html(tabJson.msg);
      $("#msg").show();
      setInterval(function(){ $("#msg").hide(); }, 3000);
    },
    error : function(err){
    }
  }); 
}

function req_enlever_panier()
{
  //alert("req_enreg_panier()");
  $.ajax({
    type: "POST",
    url: "../gestionnaire/gestionPanier.php",
    data: $("#formEnleverPanier").serialize(),
    dataType:"json",
    //dataType:  "text"  ,
    success:function(tabJson){
      //alert(JSON.stringify(tabJson));
      $("#msg").html(tabJson.msg);
      $("#msg").show();
      setInterval(function(){ $("#msg").hide(); }, 3000);
    },
    error : function(err){
    }
  }); 
}

//********************** Fonction auxiliaires **********
function lister(tabJson){//alert(JSON.stringify(tabJson));
  var rep="";
  rep+="<table>";
  var taille=tabJson.length;
  var ligne=null;
  rep+="<tr><th>ID Film</th><th>Titre</th><th>Realisateur</th><th>Categorie</th><th>Duree</th><th>Prix</th><th>Image</th></tr>";
  for(var i=0;i<taille;i++){
    ligne=tabJson[i];
    rep+="<tr><td>"+ligne['idfilm']+"</td>";
    rep+="<td>"+ligne['titre']+"</td>";
    rep+="<td>"+ligne['realisateur']+"</td>";
    rep+="<td>"+ligne['categorie']+"</td>";
    rep+="<td>"+ligne['duree']+"</td>";
    rep+="<td>"+ligne['prix']+"</td>";
    rep+="<td><a href=\"../images/image_"+ligne['image']+".jpg\"><video id="+ligne['image']+" poster=\"../images/image_"+ligne['image']+".jpg\" width='65'; height='95'></video></a></td></tr>";
    // rep+="<td><img src=\"../images/image_"+ligne['image']+".jpg\" style='width:65px;height:95px;'></td></tr>";
    
  }
  rep+="</table>";
  $("#contenu").html(rep);
  $("#fenetre").show();
}
