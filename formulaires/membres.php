<?php
// Auteur: Farzin Faridfar
// Cours: IFT1147 Programmation serveur Web avec PHP
// Date: 18 Dec 2016
// Travail paratique #3
// Fichier: membres.php
// Desc: La page formulaire des membres

session_start();
if (!isset($_SESSION['usager'])){
	echo "<br>Vous devez vous connecter pour acc&eacuteder &agrave cette page";
	echo "<br><br><a href=\"../index.html\">Retour au formulaire</a>";
}
else{//echo session_id();
?>

<!DOCTYPE html>
<html>
  <title>Travail Pratique #3</title>
  <head>
    <script language='javascript' src="../js/jquery-3.1.1.min.js"></script>
    <script language='javascript' src="../js/requetesFilm.js"></script>
    <script language='javascript' src="../js/requetesMembre.js"></script>
    <script language='javascript' src="../js/requetesPanier.js"></script>
    <link rel='stylesheet' type='text/css' href="../css/inputStyle.css">
    <link rel='stylesheet' type='text/css' href="../css/tableStyle.css">
  </head>
  <body>

   <!-- -------------------- Deconnecter -------------------- -->
    <input type='button'  value="Deconnecter" style='position:absolute;right:5%' onClick="document.formDeconnecter.submit();">
     <div id="divDeconn">
    <form   id="formDeconnecter" name="formDeconnecter" action="../gestionnaire/gestionMembre.php" method='POST'>
    <input type='hidden' name="action" value="deconnecter">
    </form>
    </div>

   <!-- -------------------- Afficher -------------------- -->
    <input type='button' class='button butAff' value="Lister" onClick="req_lister();invisible('fenetre');"><br>
 
    <form id="formLister">
      <input type="hidden" name="action" value="lister">
    </form>
    <br>

    <!-- -------------------- Afficher Panier -------------------- -->
    <input type='button' class='button butAff' value="Lister le Panier" onClick="req_lister_panier();invisible('fenetre');">
    <form id="formListerPanier">
      <input type="hidden" name="action" value="listerPanier">
    </form>
    <br>

    <!-- -------------------Enregistrer------------------ -->    
    
    <div id="divEnregPanier" style="position:absolute;top:30%">    
      <h3>Ajouter au Panier</h3>
      <form id="formEnregPanier">
	ID Film : <input type='text' class='enregIn'  id="aAjout" name="aAjout"><br>	
	<input type='hidden' name="action" value="enregistrerPanier">
	<input type='button' value="Envoyer" onClick="req_enreg_panier();vider('aAjout');">
      </form>
    </div>
    <br><br>

    <!-- -------------------- Enlever -------------------- -->    
    
    <div id="divEnlever" style="position:absolute;top:50%">
      <form id="formEnleverPanier">
	<h3>Enlever du Panier</h3>
	ID Film : <input type='text' class='suppIn'  id="aSuppPanier" name="aSuppPanier"><br>
	<input type='hidden' name="action" value="enleverPanier">
	<input type='button' value="Envoyer" onClick="req_enlever_panier();vider('aSuppPanier');">
      </form>
    </div>
    <br><br><br><br>

<!-- ----------------- Laffichage de reponse de serveur ----------------- -->
    <div id="fenetre" style="display:none;position:absolute;top:20%;left:30%">
      <div id="entete" style="float:right" onClick="$('#fenetre').hide();">[fermer]</div>
      <div id="contenu"></div>
    </div>
    <div id="msg" style="display:none;color:red;position:absolute;top:0%;left:40%"></div>
  </body>
</html>

<?php
}
?>