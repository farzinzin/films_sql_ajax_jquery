<?php
// Auteur: Farzin Faridfar
// Cours: IFT1147 Programmation serveur Web avec PHP
// Date: 18 Dec 2016
// Travail paratique #3
// Fichier: admin.php
// Desc: La page formulaire d'administrateur

session_start();
if (!isset($_SESSION['usager'])){
	echo "<br>Vous devez vous connecter pour acc&eacuteder &agrave cette page";
	echo "<br><br><a href=\"../index.html\">Retour au formulaire</a>";
}
else{//echo session_id();
?>

<!DOCTYPE html>
<html>
  <title>Travail Pratique #3</title>
  <head>
    <script language='javascript' src="../js/jquery-3.1.1.min.js"></script>
    <script language='javascript' src="../js/requetesFilm.js"></script>
    <script language='javascript' src="../js/requetesMembre.js"></script>
    <link rel='stylesheet' type='text/css' href="../css/inputStyle.css">
    <link rel='stylesheet' type='text/css' href="../css/tableStyle.css">
  </head>
  <body>

    <!-- -------------------- deconnecter -------------------- -->
    <input type='button'  value="Deconnecter" style='position:absolute;right:5%' onClick="document.formDeconnecter.submit();">    
    <div id="divDeconn">
    <form   id="formDeconnecter" name="formDeconnecter" action="../gestionnaire/gestionFilm.php" method='POST'>
    <input type='hidden' name="action" value="deconnecter">
    </form>
    </div>

    <!-- -------------------- Afficher -------------------- -->
    <input type='button' class='button butAff' value="Lister" onClick="req_lister();invisible('divEnreg');invisible('divEnlever');invisible('divModifier');">
    <form id="formLister">
      <input type="hidden" name="action" value="lister">
    </form>
    <br>

    <!-- -------------------Enregistrer------------------ -->    
    <input type='button' class='button butEnreg' value="Enregistrer" onClick="visible('divEnreg');invisible('fenetre');invisible('divEnlever');invisible('divModifier');">
    <div id="divEnreg" style="position:absolute;top:20%;left:45%;display:none">
      <form id="formEnreg">
	<h3>Enregistrer Film</h3>
	<div>
    	  Titre:<br><input type='text'  class='enregIn' id="titre" name="titre"></br>
    	  R&eacutealisateur:<br><input type='text'  class='enregIn' id="realisateur" name="realisateur"></br>
    	  Cat&eacutegorie:<br>
    	  <select class='selEnreg' id="categorie" name="categorie">
    	    <option name="action">Action</option>
    	    <option name="dram_rep">Drame et R�pertoire</option>
    	    <option name="comedie">Com�die</option>
    	    <option name="scifi">Sience Fiction</option>
    	    <option name="horreur">Horreur</option>
    	    <option name="suspense">Suspense</option>
    	    <option name="musicale">Com�dies Musicales</option>
    	    <option name="fete">Pour le temps des F�tes</option>
    	    <option name="quebecois">Qu�b�cois</option>
    	    <option name="famille">Pour la famille</option>
    	  </select><br>
    	  Dur&eacutee:<br><input type='text' class='enregIn' id="duree" name="duree"> min</br>
    	  Prix:<br><input type='text' class='enregIn' id="prix" name="prix"> $</br>
    	  Image:<br>
    	  <select class='selEnreg' id="image" name="image">
        <option name="1">1</option>
        <option name="2">2</option>
	    <option name="3">3</option>
	    <option name="4">4</option>
	    <option name="5">5</option>
        <option name="6">6</option>
        <option name="7">7</option>
	    <option name="8">8</option>
	    <option name="9">9</option>
	    <option name="10">10</option>
	  </select><br>
    	</div>
	<br>

	<input type='hidden' name="action" value="enregistrer">
	<input type='button' value="Envoyer" onClick="req_enreg();">
	<input type='reset' value="Vider">
	<input type='button' value="Cacher" onClick="invisible('divEnreg');">
      </form>
    </div>
    <br><br>

    <!-- --------------------Supprimer-------------------- -->    
    <input type='button' class='button butSupp' value="Enlever" onClick="visible('divEnlever');invisible('fenetre');invisible('divEnreg');invisible('divModifier');">
    <div id="divEnlever" style="position:absolute;top:20%;left:45%;display:none">
      <form id="formEnlever">
	<h3>Enlever un Film</h3>
	ID Film : <input type='text' class='suppIn'  id="aSupp" name="aSupp"><br>
	<input type='hidden' name="action" value="enlever">
	<input type='button' value="Envoyer" onClick="req_enlever()";>
	<input type='button' value="Cacher" onClick="invisible('divEnlever');">
      </form>
    </div>
    <br><br>
    <!-- --------------------Modifier-------------------- -->
    <input type='button' class='button butModif'  value="Mettre &agrave jour" onClick="visible('divModifier');invisible('fenetre');invisible('divEnlever');invisible('divEnreg');">
    <div id="divModifier"
	style="position:absolute;top:20%;left:45%;display:none">
      <form id="formModifier">
	<h3>Modifier un Film</h3>
	ID Film : <input type='text' class='modIn' id="aMod" name="aMod"><br>
	<input type="hidden" name="action" value="modifier">
	<input type="hidden" name="code" value="fiche">
	<input type="button" value="Envoyer" onClick="req_modifier_fiche();">
	<input type="button" value="Cacher" onClick="invisible('divModifier');">
      </form>
    </div>
    <br>

    <!-- ------------ L'affichage de reponse de serveur ------------- -->
    <div id='fenetre' style='display:none;position:absolute;top:10%;left:30%'>
      <div id='entete' style="float:right" onClick="$('#fenetre').hide();">[fermer]</div>
      <div id="contenu"></div>
    </div>
    <div id="msg" style="display:none;color:red;position:absolute;top:0%;left:40%"></div>
  </body>
</html>

<?php
}
?>