-- phpMyAdmin SQL Dump
-- version 4.1.4
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 17, 2016 at 06:16 AM
-- Server version: 5.6.15-log
-- PHP Version: 5.4.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `faridfaf_filmsbd`
--

-- --------------------------------------------------------

--
-- Table structure for table `films`
--

CREATE TABLE IF NOT EXISTS `films` (
  `idfilm` int(8) NOT NULL AUTO_INCREMENT,
  `titre` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `realisateur` varchar(75) COLLATE utf8_unicode_ci NOT NULL,
  `categorie` varchar(75) COLLATE utf8_unicode_ci NOT NULL,
  `duree` time NOT NULL,
  `prix` float NOT NULL,
  `image` tinytext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`idfilm`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=18 ;

--
-- Dumping data for table `films`
--

INSERT INTO `films` (`idfilm`, `titre`, `realisateur`, `categorie`, `duree`, `prix`, `image`) VALUES
(7, 'WALL-E', 'Andrew Stanton', 'Comédie', '01:44:00', 9.9, '1'),
(8, 'The Incredibles', 'Brad Bird', 'Action', '01:56:00', 9.9, '2'),
(9, 'Ratatouille', 'Brad Bird, Jan Pinkava', 'Action', '01:55:00', 9.9, '3'),
(10, 'Spirited Away', 'Hayao Miyazaki', 'Drame et Répertoire', '02:05:00', 9.9, '4'),
(11, 'How to Train Your Dragon', ' Dean DeBlois, Chris Sanders', 'Pour le temps des Fêtes', '01:38:00', 9.9, '5'),
(12, 'Monsters, Inc.', 'Pete Docter, Lee Unkrich, David Silverman', 'Sience Fiction', '01:32:00', 9.9, '7'),
(13, 'Inside Out', 'Pete Docter, Ronnie del Carmen', 'Comédie', '01:42:00', 9.9, '8'),
(14, 'Up', 'Pete Docter, Bob Peterson', 'Suspense', '01:41:00', 9.9, '9'),
(15, 'Zootopia', 'Byron Howard, Rich Moore', 'Sience Fiction', '01:21:00', 9.9, '10'),
(17, 'Toy Story', 'John Lasseter', 'Comédie', '01:21:00', 9.9, '6');

-- --------------------------------------------------------

--
-- Table structure for table `membres`
--

CREATE TABLE IF NOT EXISTS `membres` (
  `idmembre` int(8) NOT NULL AUTO_INCREMENT,
  `nomMem` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `passMem` varchar(75) COLLATE utf8_unicode_ci NOT NULL,
  `courrielMem` varchar(75) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`idmembre`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=24 ;

--
-- Dumping data for table `membres`
--

INSERT INTO `membres` (`idmembre`, `nomMem`, `passMem`, `courrielMem`) VALUES
(20, 'admin', 'admin', 'admin@film.com'),
(21, 'al pacino', 'pacino1940', 'al_pacino@film.com'),
(22, 'hamed', '1234567890', 'rtert'),
(23, 'hamed', '1234567890', 'rtert');

-- --------------------------------------------------------

--
-- Table structure for table `panier`
--

CREATE TABLE IF NOT EXISTS `panier` (
  `idfilm` int(8) NOT NULL DEFAULT '0',
  `titre` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `realisateur` varchar(75) COLLATE utf8_unicode_ci NOT NULL,
  `categorie` varchar(75) COLLATE utf8_unicode_ci NOT NULL,
  `duree` time NOT NULL,
  `prix` float NOT NULL,
  `image` tinytext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
