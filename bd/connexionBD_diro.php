<?php
// Auteur: Farzin Faridfar
// Cours: IFT1147 Programmation serveur Web avec PHP
// Date: 18 Dec 2016
// Travail paratique #3
// Fichier: connexionBD.php
// Desc: connection avec la base de donnee
// avec la requete prepare PDO

try {
  $dns = 'mysql:host=mysql.iro.umontreal.ca;dbname=faridfaf_filmsbd';
  $user = 'faridfaf';
  $pass = '$s3_Y5Wu5_HJyE';
 
  //Connexion options PDO::ATTR_PERSISTENT => true
  $options = array(
    PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
  );
  $conn = new PDO( $dns, $user, $pass, $options );
} catch ( Exception $e ) {
	$msgTab['response']="Server problem. Try later.";
	echo json_encode($msgTab);
	exit();
}
?>