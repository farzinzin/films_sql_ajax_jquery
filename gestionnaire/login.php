<?php
// Auteur: Farzin Faridfar
// Cours: IFT1147 Programmation serveur Web avec PHP
// Date: 18 Dec 2016
// Travail paratique #3
// Fichier: login.php
// Desc: Gerer la connection d'admin et des membres

session_start();
require_once("../bd/connexionBD.php");
$table = "membres";
$nom=$_POST['nomConn'];
$pass=$_POST['passConn'];


$req="SELECT * FROM $table WHERE nomMem=? AND passMem=?";
try {
    $stmt = $conn->prepare($req);
    $stmt->execute(array($nom,$pass));
    $result=$stmt->fetch(PDO::FETCH_ASSOC);
    
    if ($result==null){
        echo "<b>Membre Invalide<br><br><br>";
        echo "<a href=\"../index.html\">Retour au formulaire</a>";
    }
    else if($result['nomMem']=='admin')
    {
       //echo implode(",",$result);
       $_SESSION['usager']=$nom;
       //header('Location: ../formulaires/admin.html');
       header('Location: ../formulaires/admin.php');
    }
    else 
    {
        $_SESSION['usager']=$nom;
        //header('Location: ../formulaires/membres.html');
        header('Location: ../formulaires/membres.php');
    }

} 
catch(Exception $e){
    $reponse['msg']='Probleme pour envoeyr dossier';
}

?>