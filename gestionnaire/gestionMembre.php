<?php
// Auteur: Farzin Faridfar
// Cours: IFT1147 Programmation serveur Web avec PHP
// Date: 18 Dec 2016
// Travail paratique #3
// Fichier: gestionMembre.php
// Desc: La gestion des membres


require_once("../bd/connexionBD.php");
$reponse=array();

function ajouterMembre(){
    global $reponse;
    global $conn;

    $nomMem=$_POST['nomAjout'];
    $passMem=$_POST['passAjout'];
    $courrielMem=$_POST['mail'];
    
    $req="INSERT INTO membres VALUES(0,?,?,?)";
    try {
        $stmt = $conn->prepare($req);
        $stmt->execute(array($nomMem,$passMem,$courrielMem));
        $reponse['msg']='Vous avez a bien ajoute';
    } catch(Exception $e){
        $reponse['msg']='Probleme pour ajouter le membre';
        //$reponse['vals']=$numMem."/".$passMem."/".$courrielMem;
    }
}

function eliminer_panier()
{
    global $reponse;
    global $conn;
    $req = "DELETE FROM panier";
    
    try {
        $stmt = $conn->prepare($req);
        $stmt->execute();
    } catch(Exception $e){
        $reponse['msg']='Probleme pour eliminer du panier';
    }
}

// le controleur
$action=$_POST['action'];
switch($action){
case "ajouterMem" :
    ajouterMembre();
    break;
case "deconnecter":
    eliminer_panier();
    session_unset();
    session_destroy();
    header('Location: ../index.html');
    break;
}

echo json_encode($reponse);

    
?>