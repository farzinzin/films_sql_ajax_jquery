<?php
// Auteur: Farzin Faridfar
// Cours: IFT1147 Programmation serveur Web avec PHP
// Date: 18 Dec 2016
// Travail paratique #3
// Fichier: gestionFilm.php
// Desc: La gestion de CRUD pour les films

session_start();
if(!isset($_SESSION['usager']))
{
    echo "<br>Vous devez vous connecter pour acc&eacuteder � cette page";
	echo "<br><a href=\"../index.html\">Retour � la page d'accueil</a>";
}
else{

    require_once("../bd/connexionBD.php");
    $reponse=array();

    function lister(){
        global $reponse;
        global $conn;
        $req="SELECT * FROM films";
        try {
            $stmt = $conn->prepare($req);
            $stmt->execute();
            $i=0;
            while($row=$stmt->fetch(PDO::FETCH_ASSOC)){
                $reponse[$i]=array();
                $reponse[$i]['idfilm']=$row['idfilm'];
                $reponse[$i]['titre']=$row['titre'];
                $reponse[$i]['realisateur']=$row['realisateur'];
                $reponse[$i]['categorie']=$row['categorie'];
                $reponse[$i]['duree']=$row['duree'];
                $reponse[$i]['prix']=$row['prix'];
                $reponse[$i]['image']=$row['image'];
                $i++;
            }
        } catch(Exception $e){
            $reponse['msg']='Probleme pour lister';
        }
    }

        function enregistrer(){
            global $reponse;
            global $conn;
            $titre=$_POST['titre'];
            $realisateur=$_POST['realisateur'];
            $categorie=$_POST['categorie'];
            $duree=$_POST['duree'];
            $prix=$_POST['prix'];
            $image=$_POST['image'];
            $req="INSERT INTO films VALUES(0,?,?,?,?,?,?)";
            try {
                $stmt = $conn->prepare($req);
                $stmt->execute(array($titre,$realisateur,$categorie,$duree,$prix,$image));
                $reponse['msg']='Le film a bien enregistre';
            } catch(Exception $e){
                $reponse['msg']='Probleme pour enregistrer';
            }
        }

        function enlever(){
            global $reponse;
            global $conn;
            $aSupp=$_POST['aSupp'];
            $req = "DELETE FROM films WHERE idfilm=?";
    
            try {
                $stmt = $conn->prepare($req);
                $stmt->execute(array($aSupp));
                $reponse['msg']='Le film a bien enleve';
            } catch(Exception $e){
                $reponse['msg']='Probleme pour enlever';
            }
        }

        function envoyerDossier($idfilm){
            global $reponse;
            global $conn;
            $req="SELECT * FROM films WHERE idfilm=?";
            try {
                $stmt = $conn->prepare($req);
                $stmt->execute(array($idfilm));
                $reponse=$stmt->fetch(PDO::FETCH_ASSOC);
            } catch(Exception $e){
                $reponse['msg']='Probleme pour envoeyr dossier';
            }
        }

        function mettreAJour(){
            global $reponse;
            global $conn;
    
            $idfilm=$_POST['idmaj'];
            $titre=$_POST['titre'];
            $realisateur=$_POST['realisateur'];
            $categorie=$_POST['categorie'];
            $duree=$_POST['duree'];
            $prix=$_POST['prix'];
            $image=$_POST['image'];
	
            $req="UPDATE films SET titre=?,realisateur=?,categorie=?,duree=?,prix=?,image=? WHERE idfilm=?";

            try {
                $stmt = $conn->prepare($req);
                $stmt->execute(array($titre,$realisateur,$categorie,$duree,$prix,$image,$idfilm));
                $reponse['msg']="Le film a bien modifie";
            } catch(Exception $e){
                $reponse['msg']='Probleme pour modifier';
            }
        }

        //le controleur
        $action=$_POST['action'];
    switch($action){
    case "enregistrer" :
        enregistrer();
        break;
    case "lister" :
        lister();
        break;
    case "enlever" :
        enlever();
        break;
    case "modifier" :
        $code=$_POST['code'];
        if ($code=="fiche")
            envoyerDossier($_POST['aMod']);
        else
            mettreAJour();
        break;
    case "deconnecter":
           session_unset();
           session_destroy();
           header('Location: ../index.html');
       break;
}
    echo json_encode($reponse);

    // if($action!="deconnecter")
    // {
    //    global $conn;
    //    $conn = null;
    //    echo "<br><br><a href=\"../formulairesMembres/admin.php\">Retour au formulaire</a>";
    // }

}//fin de else de session
?>