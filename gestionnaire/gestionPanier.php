<?php
// Auteur: Farzin Faridfar
// Cours: IFT1147 Programmation serveur Web avec PHP
// Date: 18 Dec 2016
// Travail paratique #3
// Fichier: gestionPanier.php
// Desc: La gestion de CRUD pour le panier d'achat des membres

session_start();
if(!isset($_SESSION['usager']))
{
    echo "<br>Vous devez vous connecter pour acc&eacuteder � cette page";
	echo "<br><a href=\"../index.html\">Retour � la page d'accueil</a>";
}
else{

    require_once("../bd/connexionBD.php");
    $reponse=array();

        function listerPanier(){
            global $reponse;
            global $conn;
            $req="SELECT * FROM panier";
            try {
                $stmt = $conn->prepare($req);
                $stmt->execute();
                $i=0;
                while($row=$stmt->fetch(PDO::FETCH_ASSOC)){
                    $reponse[$i]=array();
                    $reponse[$i]['idfilm']=$row['idfilm'];
                    $reponse[$i]['titre']=$row['titre'];
                    $reponse[$i]['realisateur']=$row['realisateur'];
                    $reponse[$i]['categorie']=$row['categorie'];
                    $reponse[$i]['duree']=$row['duree'];
                    $reponse[$i]['prix']=$row['prix'];
                    $reponse[$i]['image']=$row['image'];
                    $i++;
                }
            } catch(Exception $e){
                $reponse['msg']='Probleme pour lister';
            }
        }

        function ajouterPanier()
        {
            global $reponse;
            global $conn;
            $aAjout=$_POST['aAjout'];
            $req="INSERT INTO panier SELECT * FROM films WHERE idfilm=?";

            try {
                $stmt = $conn->prepare($req);
                $stmt->execute(array($aAjout));
                $reponse['msg']='Le film a bien ajoute a votre panier';
            } catch(Exception $e){
                $reponse['msg']='Probleme pour ajouter au panier';
            }
        }

        function enleverPanier()
        {
            global $reponse;
            global $conn;
            $aSuppPanier=$_POST['aSuppPanier'];
            $req = "DELETE FROM panier WHERE idfilm=?";
    
            try {
                $stmt = $conn->prepare($req);
                $stmt->execute(array($aSuppPanier));
                $reponse['msg']='Le film a bien enleve de votre panier';
            } catch(Exception $e){
                $reponse['msg']='Probleme pour enlever du panier';
            }
        }

        //le controleur
        $action=$_POST['action'];
    switch($action){
    case "enregistrerPanier" :
        ajouterPanier();
        break;
    case "listerPanier" :
        listerPanier();
        break;
    case "enleverPanier" :
        enleverPanier();
        break;
    case "deconnecter":
        eliminer_panier();
        session_unset();
        session_destroy();
        header('Location: ../index.html');
        break;

}
    echo json_encode($reponse);

    // if($action!="deconnecter")
    // {
    //    global $conn;
    //    $conn = null;
    //    echo "<br><br><a href=\"../formulairesMembres/admin.php\">Retour au formulaire</a>";
    // }

}//fin de else de session
?>